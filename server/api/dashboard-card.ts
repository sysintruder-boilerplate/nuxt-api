export default defineEventHandler((_) => {
  return [
    {
      title: 'Revenue',
      desc: 'Product Sales + Services & Others - COGS',
      status: 'good',
      diff: 'inc',
      percentage: 16.4,
      total: 74.4,
      currency: true,
    },
    {
      title: 'Expense',
      desc: 'M&P + General & Administrative + Depreciation + Interest',
      status: 'bad',
      diff: 'inc',
      percentage: 10.6,
      total: 27.8,
      currency: true,
    },
    {
      title: 'Profit',
      desc: 'Revenue - Expenses',
      status: 'good',
      diff: 'inc',
      percentage: 12.2,
      total: 46.6,
      currency: true,
    },
    {
      title: 'Net Income',
      desc: 'Profit - Tax',
      status: 'good',
      diff: 'inc',
      percentage: 11.1,
      total: 43.3,
      currency: true,
    },
  ]
})
